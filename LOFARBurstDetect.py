import numpy as np
import cv2
import json
from scipy import signal
from astropy.io import fits
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from matplotlib import cm
import matplotlib.dates as plt_dates
from datetime import datetime
import kornia as K


def normalize_image(img):
    smin = 0
    smax = 255
    return (img - img.min()) * (smax - smin) / (img.max() - img.min()) + smin


def bck_sub_normalize(flux, flux_lim):
    start_mean = flux.mean()
    while True:
        temp_Data_I = flux - np.mean(flux, axis=0)
        print(np.mean(flux, axis=0).shape)
        flux = temp_Data_I.clip(min=-5)
        if (((start_mean - flux.mean())/start_mean) >= flux_lim) & (flux.mean() > flux_lim):
            start_mean = flux.mean()
        else:
            break
    flux = ((flux - flux.min())/(flux.max()-flux.min()))
    return flux


def adaptive_threshold(image, area_thres, brush_width):
    # Load image, grayscale, Gaussian blur, Adaptive threshold
    blur = cv2.GaussianBlur(image.astype('uint8'), (1, 9), 0)
    thresh = cv2.adaptiveThreshold(
        blur, 100, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 43, 3)

    # Find contours and filter using contour area
    cnts = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    cnts = cnts[0] if len(cnts) == 2 else cnts[1]
    big_cnts = []
    for c in cnts:
        area = cv2.contourArea(c)
        if area > area_thres:
            xpts = np.vstack(c)[:, 0]
            ypts = np.vstack(c)[:, 1]
            if ((xpts.max()-xpts.min()) > 20) and (ypts.max()-ypts.min()) < 25:
                continue
            if (ypts.max()-ypts.min()) < 25:
                continue
            if ypts.max() > 100:
                print(ypts.max())
                cv2.drawContours(
                    image, [c], -1, (255, 255, 12), brush_width, 1, 0, 0)
                big_cnts.append(c)
    return image, cnts, big_cnts


def conv_step(_image, pixel_map, cnt_map, x_pix__ref, y_pix__ref, width_x, height_y,
              scale_x, scale_y):

    #assert y_pix__ref < (_image.shape[0]-height_y//2)
    assert y_pix__ref > height_y//2
    #assert x_pix__ref < (_image.shape[0]-width_x//2)
    assert x_pix__ref > width_x//2

    xcen, ycen = x_pix__ref, y_pix__ref
    ll, rr, tt, bb = ((xcen-width_x//2), (xcen+width_x//2),
                      (ycen-height_y//2), (ycen+height_y//2))
    LL, RR, TT, BB = ((ll-(scale_x*(width_x//2))),
                      (rr+(scale_x*(width_x//2))),
                      (tt-(scale_y*(height_y//2))),
                      (bb+(scale_y*(height_y//2))))

    if LL < 0:
        LL = 0
    if RR > _image.shape[1]:
        RR = _image.shape[1]-1
    if TT < 0:
        TT = 0
    if BB > _image.shape[0]:
        BB = _image.shape[0]-1

    # print(ll,rr,tt,bb)
    # print(LL,RR,TT,BB)

    conv_patch = normalize_image(signal.convolve2d(pixel_map[TT:BB, LL:RR],
                                                   cnt_map[tt:bb, ll:rr],
                                                   boundary='symm', mode='same'))
    _image[TT:BB, LL:RR] = _image[TT:BB, LL:RR] + conv_patch

    return _image


def load_and_preprocess():
    norm_data = bck_sub_normalize(hdulist[0].data, 0.01).T[::-1]
    im = np.array(norm_data, dtype=np.uint8)
    im = cv2.fastNlMeansDenoising(im, None, 6, 1, 3)
    im = np.array(im, dtype=np.uint8)
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (1, 3))
    opening = cv2.morphologyEx(im, cv2.MORPH_OPEN, kernel)
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (1, 101))
    closing = cv2.morphologyEx(opening, cv2.MORPH_CLOSE, kernel)
    img = normalize_image(closing)
    pixel_map = np.copy(img.astype('float'))
    pixel_map[pixel_map >= 50] = 255
    pixel_deci = np.log10(pixel_map)
    pixel_deci[pixel_deci == -np.Inf] = 0
    pixel_deci[(pixel_deci >= 30)] = 0
    pixel_deci[(pixel_deci <= -5)] = 0
    pixel_deci[(pixel_deci >= 0.01) & (pixel_deci < 10)] = 255  # -0.5 to 2.5
    _, cnts, _ = adaptive_threshold(pixel_deci.copy(), 1, 3)

    cnt_image = np.zeros_like(norm_data)
    for cnt in cnts:
        xpts = np.vstack(cnt)[:, 0]
        ypts = np.vstack(cnt)[:, 1]
        if ((xpts.max()-xpts.min()) > 20) and (ypts.max()-ypts.min()) < 25:
            continue
        if (ypts.max()-ypts.min()) < 25:
            continue
        if ypts.max() > 100:
            cnt_image[ypts, xpts] = 255
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (1, 3))
    cnt_image = cv2.morphologyEx(cnt_image, cv2.MORPH_CLOSE, kernel)

    build_image = np.zeros_like(norm_data)
    width, height = 2, 17
    x_ref = 15
    y_pos = build_image.shape[0]
    y_step = 10
    while(y_pos > height):
        y_pos = y_pos - y_step
        for x_step in np.arange(1, 750, 10):
            x_pos = x_ref+x_step
            build_image = conv_step(build_image, pixel_deci, cnt_image, x_pos, y_pos, width_x=width, height_y=height,
                                    scale_x=2, scale_y=2)

    build_image = np.nan_to_num(build_image)
    #build_image[build_image<50] = 0
    # build_image[(build_image>50)&(build_image<1000)]=150
    #build_image[build_image>1000] = 255

    extracted_image, _, big_cnts = adaptive_threshold(
        build_image.copy(), 500, 3)
