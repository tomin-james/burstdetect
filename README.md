# BurstDetect

A module containing a set of algorithms to detect and segment radio bursts originating from the Sun from a variety of
ground-based radio telescopes. 

Detecting solar radio-bursts is tricky because of the high level of background noise and the varing scales and sizes.
The functionalities here are for two specific types of bursts. Type III radio bursts with interplanetary components and 
type I radio noise storm bursts.

A traditional deep learning approach was not used, because of the lack of labeled data and the strong priors which was known
regarding the bursts. 

![alt text](imgs/1.png "Timeline of an event")
The above figure shows extraction of type III burst. The spectrogram was constructed by combining four different instruments covering frequency
range 1 GHz - 0.1 kHz. The tracking of type III bursts gives important information regarding the acceleration and propagation of electrons from the solar
site. The algorithm for this currently is not real time owing to the time consuming convolution process involved. 
![alt text](imgs/2.png "Timeline of an event")

![alt text](imgs/3.png "Timeline of an event")
The above two figures depict detection of type III showers and type I bursts using LOFAR High Band Antenna(HBA).
Because of the high data throughput of modern radio astronomy instruments, it is important to have reliable algorithms
that can detect the necessary features and remove the rest of the data.
We are currently in the stage of optimizing the algorithms to work in real time during observation campaigns using LOFAR telescope.

