import numpy as np
import cv2
import json
from scipy import signal
from astropy.io import fits
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from matplotlib import cm
import matplotlib.dates as plt_dates
from datetime import datetime


def file_load(file_path):

    img, hdr = fits.getdata(file_path, header=True)
    try:
        f = open(file_path.split('.')[0]+'.json')
        json_info = json.load(f)
        f.close()
        return img, json_info
    except:
        return img
