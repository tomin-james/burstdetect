from abc import ABC, abstractmethod


class Instrument_Meta(ABC):
    '''
    The base class to record the metadata of instruments.
    Currently this protocol is not strictly enforced.


    Any new instrument should inherit from this class.
    '''

    @property
    @abstractmethod
    def launch_date(self):
        return self._launch_date

    @property
    @abstractmethod
    def space_based(self):
        return self._space_based

    # @property
    # @abstractmethod
    # def measurement_unit(self):
    #     return self._measurment_unit

    @property
    @abstractmethod
    def sensing_type(self):
        return self._sensing_type

    @property
    @abstractmethod
    def end_of_life(self):
        return self._end_of_life

    @property
    @abstractmethod
    def data_range(self):
        return self._data_range

    @property
    @abstractmethod
    def summarise(self):
        return self._summarise


class Instrument_Func(ABC):
    '''
    The two abstractmethods here are the major
    workforce of the instruments.

    fetch_data : Acquires mission specific data from DataSources

    plot_data : Plot functions for creating mission specific plots
    '''

    @abstractmethod
    def fetch_data(self):
        pass

    @abstractmethod
    def plot_data(self):
        pass
