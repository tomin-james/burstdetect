from HelioEx.TimeLine import TimeLine
from HelioEx.utils import Utility
import numpy as np
from scipy.signal import find_peaks
import itertools
import matplotlib.dates as plt_dates
import astropy.units as u


class PhysicalEvent(TimeLine):

    '''
    Class to be used to create a physical event
    '''

    def __init__(self, dt_, quant_used, config):
        super().__init__(dt_, config=config)
        self.quant_used = quant_used
        self.instantiate_instruments()
        self.get_data()

    def lightcurve_max(self, prominence=0.3, window_size=19, width=20):
        self.peaks = {}
        self.time_peaks = {}
        self.peak_times = {}
        for instr_client in self.activated_instrs.keys():
            self.time_peaks[instr_client] = []
            _idx = Utility.find_time_idx(
                self.time_range, np.asarray(self.data[instr_client]['time']))
            smoothed_data = np.apply_along_axis(
                Utility.smooth, 0, self.data[instr_client]['data'][_idx, :], window_size)

            if self.activated_instrs[instr_client]._plot_type == 'lightcurve':
                smoothed_data = Utility.lightcurve_normalise(smoothed_data)

            if self.activated_instrs[instr_client]._plot_type == 'spectrogram':
                smoothed_data = Utility.bck_sub_normalize(smoothed_data)
                smoothed_data = np.asarray(np.array_split(smoothed_data, int(smoothed_data.shape[1]/10),
                                                          axis=1)).mean(axis=2).T

            self.peaks[instr_client], _ = np.apply_along_axis(
                find_peaks, 0, smoothed_data, prominence=prominence, width=width)
            for peak_grp in self.peaks[instr_client]:
                self.time_peaks[instr_client].append(plt_dates.date2num(
                    self.data[instr_client]['time'][_idx])[peak_grp])
            combined_time_peaks = list(
                itertools.chain.from_iterable(self.time_peaks[instr_client]))
            bin_val, bin_loc = np.histogram(combined_time_peaks, bins=int((self.time_range.minutes/u.minute)/5),
                                            range=(plt_dates.date2num(self.time_range.start.datetime),
                                                   plt_dates.date2num(self.time_range.end.datetime)))
            bin_val[bin_val < bin_val.max()*0.6] = 0
            bin_val[bin_val > 0] = 1
            _bins = bin_loc[:-1]*bin_val
            self.peak_times[instr_client] = plt_dates.num2date(
                _bins[_bins != 0])

    def adaptive_threshold(image, area_thres):
        # Load image, grayscale, Gaussian blur, Adaptive threshold
        blur = cv2.GaussianBlur(image.astype('uint8'), (9, 9), 0)
        thresh = cv2.adaptiveThreshold(
            blur, 200, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 43, 3)

        # Find contours and filter using contour area
        cnts = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        cnts = cnts[0] if len(cnts) == 2 else cnts[1]
        big_cnts = []
        for c in cnts:
            area = cv2.contourArea(c)
            if area > area_thres:
                xpts = np.vstack(c)[:, 0]
                ypts = np.vstack(c)[:, 1]
                if ((xpts.max()-xpts.min()) > 200) and (ypts.max() > 500):
                    continue
                if ((xpts.max()-xpts.min()) < 50) or ((ypts.max()-ypts.min()) > 130):
                    cv2.drawContours(
                        image, [c], -1, (255, 255, 12), 1, 1, 0, 0)
                big_cnts.append(c)
        return image, cnts, big_cnts

    def conv_step(_image, pixel_map, cnt_map, x_pix__ref, y_pix__ref, width_x, height_y,
                  scale_x, scale_y):

        assert y_pix__ref < (_image.shape[0]-height_y//2)
        assert y_pix__ref > height_y//2
        assert x_pix__ref < (_image.shape[0]-width_x//2)
        assert x_pix__ref > width_x//2

        xcen, ycen = x_pix__ref, y_pix__ref
        ll, rr, tt, bb = ((xcen-width_x//2), (xcen+width_x//2),
                          (ycen-height_y//2), (ycen+height_y//2))
        LL, RR, TT, BB = ((ll-(scale_x*(width_x//2))),
                          (rr+(scale_x*(width_x//2))),
                          (tt-(scale_y*(height_y//2))),
                          (bb+(scale_y*(height_y//2))))

        if LL < 0:
            LL = 0
        if RR > _image.shape[1]:
            RR = _image.shape[1]-1
        if TT < 0:
            TT = 0
        if BB > _image.shape[0]:
            BB = _image.shape[0]-1

        # print(ll,rr,tt,bb)
        # print(LL,RR,TT,BB)

        conv_patch = normalize_image(signal.convolve2d(pixel_map[TT:BB, LL:RR],
                                                       cnt_map[tt:bb, ll:rr],
                                                       boundary='symm', mode='same'))
        _image[TT:BB, LL:RR] = _image[TT:BB, LL:RR] + conv_patch

        return _image
