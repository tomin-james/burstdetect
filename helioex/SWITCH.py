class TurnOn:

    '''
    A wrapper class which will be embeded within on all instrument class
    as a means to register the instrument class within the namespace.
    Only the instruments activated using this class will be available for data gathering.
    '''
    print('I switch things on')
