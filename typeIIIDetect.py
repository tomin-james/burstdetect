from scipy import signal
from scipy import misc
import cv2
import numpy as np


def normalize_image(img):
    smin = 0
    smax = 255
    return (img - img.min()) * (smax - smin) / (img.max() - img.min()) + smin


def bck_sub_normalize(flux, flux_lim):
    start_mean = flux.mean()
    while True:
        temp_Data_I = flux - np.mean(flux, axis=0)
        print(np.mean(flux, axis=0).shape)
        flux = temp_Data_I.clip(min=-5)
        if (((start_mean - flux.mean())/start_mean) >= flux_lim) & (flux.mean() > flux_lim):
            start_mean = flux.mean()
        else:
            break
    flux = ((flux - flux.min())/(flux.max()-flux.min()))
    return flux


def normalize_image(img):
    smin = 0
    smax = 255
    return (img - img.min()) * (smax - smin) / (img.max() - img.min()) + smin


def merge_image(data_dict):
    refer_freq_no = data_dict['nda'].shape[1]
    insts = list(data_dict.keys())
    print(insts)
    for inst in insts:
        data_dict[inst] = normalize_image(resize(data_dict[inst],
                                                 (data_dict[inst].shape[0],
                                                  refer_freq_no),
                                                 anti_aliasing=True))
    return np.vstack((data_dict[insts[1]],
                      data_dict[insts[0]],
                      data_dict[insts[2]],
                      data_dict[insts[3]]))


def create_spec_object(*, time_dict, **kwargs):
    kwargs['insts'] = insts_demand
    kwargs['config'] = config
    config_ = {'at_home': config['at_home']}

    fetch_function = {
        'callisto': fetch_callisto,
        'orfee': fetch_orfee,
        'nda': fetch_nda,
        'waves': fetch_waves,
    }

    fetch_data = {}
    time_sliced_flux = {}
    freqs = {}

    output_directory = './data'

    dd, mm, yyyy = time_dict['date start']
    hh_st, MM_st, ss_st = time_dict['time start']
    dd_en, mm_en, yyyy_en = time_dict['date start']
    hh_en, MM_en, ss_en = time_dict['time end']

    sunpy_date = str(yyyy)+'-'+str(mm).zfill(2)+'-'+str(dd).zfill(2)

    tr = TimeRange([sunpy_date+' '+str(hh_st)+':'+str(MM_st),
                    sunpy_date+' '+str(hh_en)+':'+str(MM_en)])
    print(tr)

#     for inst in insts_demand:
#         try:
#             fetch_data[inst] = fetch_function[inst](tr,config)
#         except:
#             print(inst,' data not available.')
    for inst in insts_demand:
        fetch_data[inst] = fetch_function[inst](tr, config=config_)
    print('Downloaded data for the following instruments:', fetch_data.keys())

    if 'waves' in fetch_data.keys():
        waves_timer1 = fetch_data['waves']['time_r1']
        waves_timer2 = fetch_data['waves']['time_r2']
        waves_idx1 = np.where((waves_timer1 >= tr.start.datetime) &
                              (waves_timer1 <= tr.end.datetime))[0]
        waves_idx2 = np.where((waves_timer2 >= tr.start.datetime) &
                              (waves_timer2 <= tr.end.datetime))[0]

        fetch_data['waves']['rad2_flux'] = np.nan_to_num(
            fetch_data['waves']['rad2_flux'])*255
        if fetch_data['waves']['rad2_flux'][:, waves_idx2].max() < 1:
            fetch_data['waves']['rad2_flux'][:, waves_idx2] = \
                fetch_data['waves']['rad2_flux'][:, waves_idx2]*(255.0/fetch_data['waves']['rad2_flux']
                                                                 [:, waves_idx2].max())
        time_sliced_flux['waves_rad2'] = fetch_data['waves']['rad2_flux'][:, waves_idx2]

        fetch_data['waves']['rad1_flux'] = np.nan_to_num(
            fetch_data['waves']['rad1_flux'])*255
        if (fetch_data['waves']['rad1_flux'][:, waves_idx1].max() < 1) & \
                (fetch_data['waves']['rad1_flux'][:, waves_idx1].max() > 0):
            fetch_data['waves']['rad1_flux'][:, waves_idx1] = \
                fetch_data['waves']['rad1_flux'][:, waves_idx1]*(255.0 /
                                                                 fetch_data['waves']['rad1_flux']
                                                                 [:, waves_idx1].max())
        time_sliced_flux['waves_rad1'] = fetch_data['waves']['rad1_flux'][:, waves_idx1]
        freqs['waves_rad1'] = np.linspace(0.02, 1.04, 256)
        freqs['waves_rad2'] = np.linspace(1.075, 13.825, 256)

    if 'nda' in fetch_data.keys():
        nda_time = fetch_data['nda']['time'].astype(datetime)
        nda_idx = np.where((nda_time >= tr.start.datetime) &
                           (nda_time <= tr.end.datetime))[0]
        flux_nda = bck_sub_normalize(fetch_data['nda']['flux'])
        time_sliced_flux['nda'] = flux_nda.T[:, nda_idx]
        freqs['nda'] = fetch_data['nda']['frequency']
    if 'callisto' in fetch_data.keys():
        bck_sub = fetch_data['callisto'].sliceTimeAxis(tr.start.datetime.strftime('%H:%M:%S'),
                                                       tr.end.datetime.strftime('%H:%M:%S')).subtractBackground()

        startDate = pycutils.toDate(bck_sub.imageHeader['DATE-OBS'])
        startTime = pycutils.toTime(bck_sub.imageHeader['TIME-OBS'])

        startTime = datetime.combine(
            startDate, startTime)  # make a datetime object
        endTime = pycutils.toTime(bck_sub.imageHeader['TIME-END'])
        endTime = datetime.combine(startDate, endTime)

        # get the frequencies
        # these are true frequencies
        freqs['callisto'] = np.flip(bck_sub.binTableHdu.data['frequency'][0])

        # set the limitplacehlder_arr[:,st_idx:st_idx+3600].shapes for plotting
        xLims = [startTime, endTime]
        xLims = plt_dates.date2num(xLims)  # dates to numeric values
        cal_flux = bck_sub.imageHdu.data[::-1]
        time_sliced_flux['callisto'] = bck_sub_normalize(cal_flux.T).T
    if 'orfee' in fetch_data.keys():
        orf_idx = np.where((fetch_data['orfee']['time'] >= tr.start.datetime) &
                           (fetch_data['orfee']['time'] <= tr.end.datetime))[0]
        time_sliced_flux['orfee'] = bck_sub_normalize(
            fetch_data['orfee']['flux'][orf_idx, :]).T
        freqs['orfee'] = fetch_data['orfee']['freq']

    return time_sliced_flux, freqs, nda_time[nda_idx]


def adaptive_threshold(image, area_thres):
    # Load image, grayscale, Gaussian blur, Adaptive threshold
    blur = cv2.GaussianBlur(image.astype('uint8'), (9, 9), 0)
    thresh = cv2.adaptiveThreshold(
        blur, 200, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 43, 3)

    # Find contours and filter using contour area
    cnts = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    cnts = cnts[0] if len(cnts) == 2 else cnts[1]
    big_cnts = []
    for c in cnts:
        area = cv2.contourArea(c)
        if area > area_thres:
            xpts = np.vstack(c)[:, 0]
            ypts = np.vstack(c)[:, 1]
            if ((xpts.max()-xpts.min()) > 200) and (ypts.max() > 500):
                continue
            if ((xpts.max()-xpts.min()) < 50) or ((ypts.max()-ypts.min()) > 130):
                cv2.drawContours(image, [c], -1, (255, 255, 12), 1, 1, 0, 0)
            big_cnts.append(c)
    return image, cnts, big_cnts


def conv_step(_image, pixel_map, cnt_map, x_pix__ref, y_pix__ref, width_x, height_y,
              scale_x, scale_y):

    assert y_pix__ref < (_image.shape[0]-height_y//2)
    assert y_pix__ref > height_y//2
    assert x_pix__ref < (_image.shape[0]-width_x//2)
    assert x_pix__ref > width_x//2

    xcen, ycen = x_pix__ref, y_pix__ref
    ll, rr, tt, bb = ((xcen-width_x//2), (xcen+width_x//2),
                      (ycen-height_y//2), (ycen+height_y//2))
    LL, RR, TT, BB = ((ll-(scale_x*(width_x//2))),
                      (rr+(scale_x*(width_x//2))),
                      (tt-(scale_y*(height_y//2))),
                      (bb+(scale_y*(height_y//2))))

    if LL < 0:
        LL = 0
    if RR > _image.shape[1]:
        RR = _image.shape[1]-1
    if TT < 0:
        TT = 0
    if BB > _image.shape[0]:
        BB = _image.shape[0]-1

    # print(ll,rr,tt,bb)
    # print(LL,RR,TT,BB)

    conv_patch = normalize_image(signal.convolve2d(pixel_map[TT:BB, LL:RR],
                                                   cnt_map[tt:bb, ll:rr],
                                                   boundary='symm', mode='same'))
    _image[TT:BB, LL:RR] = _image[TT:BB, LL:RR] + conv_patch

    return _image
